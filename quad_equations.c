
#include <stdio.h>
#include <math.h>

double calculate_discriminant(double a, double b, double c) {
    return b * b - 4 * a * c;
}

void find_roots(double a, double b, double c) {
    double discriminant = calculate_discriminant(a, b, c);

    if (discriminant > 0) {
        double root1 = (-b + sqrt(discriminant)) / (2 * a);
        double root2 = (-b - sqrt(discriminant)) / (2 * a);
        printf("Roots are real and different.\n");
        printf("Root 1 = %.2lf\n", root1);
        printf("Root 2 = %.2lf\n", root2);
    } else if (discriminant == 0) {
        double root = -b / (2 * a);
        printf("Roots are real and equal.\n");
        printf("Root = %.2lf\n", root);
    } else {
        double realPart = -b / (2 * a);
        double imaginaryPart = sqrt(-discriminant) / (2 * a);
        printf("Roots are complex.\n");
        printf("Root 1 = %.2lf + %.2lfi\n", realPart, imaginaryPart);
        printf("Root 2 = %.2lf - %.2lfi\n", realPart, imaginaryPart);
    }
}

int main() {
    double a, b, c;

    printf("Enter coefficients a, b, and c: ");
    if (scanf("%lf %lf %lf", &a, &b, &c) != 3) {
        printf("Invalid input.\n");
        return 1;
    }

    if (a == 0) {
        printf("Invalid quadratic equation. Coefficient a cannot be zero.\n");
        return 1;
    }

    find_roots(a, b, c);

    return 0;
}
